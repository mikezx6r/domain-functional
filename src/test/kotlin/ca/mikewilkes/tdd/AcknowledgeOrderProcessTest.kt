package ca.mikewilkes.tdd

import arrow.core.partially1
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe

class AcknowledgeOrderProcessTest : FreeSpec({
    val coalSuccess: CreateOrderAcknowledgementLetter = { HtmlString("Letter to recipient") }
    val soaSuccess: SendOrderAcknowledgement = { SendResult.Sent }

    val acknowledgeOrderDI = acknowledgeOrder.partially1(coalSuccess).partially1(soaSuccess)

    val pricedOrder = TestModels.validPricedOrder.getOrFail()

    "Acknowledge Order succeeds" - {
        "Acknowledge successfully" {
            val result = acknowledgeOrderDI(pricedOrder)

            result.shouldNotBeNull()
            result.orderId shouldBe pricedOrder.orderId
            result.emailAddress shouldBe pricedOrder.customerInfo.email
        }
    }

    "Acknowledge Order fails" {
        val soaFailure: SendOrderAcknowledgement = { SendResult.NotSent }

        val result = acknowledgeOrder(coalSuccess, soaFailure, pricedOrder)

        result.shouldBeNull()
    }
})