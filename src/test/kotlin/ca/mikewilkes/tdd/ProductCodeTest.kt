package ca.mikewilkes.tdd

import ca.mikewilkes.tdd.OrderLine.ProductCode.Gadget
import ca.mikewilkes.tdd.OrderLine.ProductCode.Widget
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidProductCode
import io.kotest.core.spec.style.FreeSpec
import io.kotest.data.row
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf

class ProductCodeTest : FreeSpec({
    "fails to build Product" - {
        listOf(
            row("null code", null, "Code must have a value"),
            row("empty", "", "Code must have a value"),
            row("blank", "  ", "Code must have a value"),
            row("Not a Widget or Gadget", "T1234", "Unknown Product code: 'T1234'.")
        ).map { (description, code: String?, message: String) ->
            description {
                verifyError(code, message)
            }
        }
    }
    "Widgets" - {
        "build successfully" {
            val code = "W1234"
            val pc = OrderLine.ProductCode(code)

            pc.shouldBeValue()
                .shouldBeInstanceOf<Widget>()
                .code shouldBe code
        }
        "fails build" - {
            listOf(
                row("Code too short", "W123", "Widget code must start with 'W' followed by 4 digits. 'W123'"),
                row("Code too long", "W12345", "Widget code must start with 'W' followed by 4 digits. 'W12345'"),
                row(
                    "alpha characters not allowed",
                    "W12A4",
                    "Widget code must start with 'W' followed by 4 digits. 'W12A4'"
                )
            ).map { (description, code: String?, message: String) ->
                description {
                    verifyError(code, message)
                }
            }
        }
    }

    "Gadgets" - {
        "build successfully" {
            val code = "G123"
            val pc = OrderLine.ProductCode(code)

            pc.shouldBeValue()
                .shouldBeInstanceOf<Gadget>()
                .code shouldBe code
        }
        "fails build" - {
            listOf(
                row("Code too short", "G12", "Gadget code must start with 'G' followed by 3 digits. 'G12'"),
                row("Code too long", "G1234", "Gadget code must start with 'G' followed by 3 digits. 'G1234'"),
                row(
                    "alpha characters not allowed",
                    "G12A",
                    "Gadget code must start with 'G' followed by 3 digits. 'G12A'"
                )
            ).map { (description, code: String?, message: String) ->
                description {
                    verifyError(code, message)
                }
            }
        }
    }
})

private suspend fun verifyError(code: String?, message: String) {
    val pc = OrderLine.ProductCode(code)

    pc.shouldBeError()
        .shouldBeInstanceOf<InvalidProductCode>()
        .also {
            it.message shouldBe message
        }
}