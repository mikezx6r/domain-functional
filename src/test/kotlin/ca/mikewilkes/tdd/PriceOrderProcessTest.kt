package ca.mikewilkes.tdd

import arrow.core.continuations.effect
import arrow.core.partially1
import arrow.core.right
import arrow.core.toNonEmptyListOrNull
import arrow.core.traverse
import ca.mikewilkes.tdd.OrderLine.OrderQuantity
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.PricingError.PriceNotInCatalog
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith
import java.math.BigDecimal

class PriceOrderProcessTest : FreeSpec({

    "Test traverse on list" {
        val t = listOf(1, 2, 3).traverse { i ->
            i.right()
        }

        t.shouldBeRight()
            .shouldHaveSize(3)
    }
    "Test travers on NEL" {
        val fromList = listOf(1, 2, 3).toNonEmptyListOrNull()
            ?: throw IndexOutOfBoundsException("Empty list doesn't contain element at index 0.")
        val t = fromList.traverse { it.right() }

        t.shouldBeRight()
            .shouldHaveSize(3)
    }
    val priceForOrders = Price(PRODUCT_PRICE_FOR_TESTS)

    val validatedOrder = TestModels.validValidatedOrder.getOrFail()
    val gppSuccess: GetProductPrice = { effect { priceForOrders } }

    val priceOrderDI = ::priceOrder.partially1(gppSuccess)

    "Price Order succeeds" - {
        "Pricing successfully" {
            val result = priceOrderDI(validatedOrder)

            result.shouldBeValue()
                .also {
                    it.orderId shouldBe validatedOrder.orderId
                    it.customerInfo shouldBe validatedOrder.customerInfo
                    it.shippingAddress shouldBe validatedOrder.shippingAddress
                    it.billingAddress shouldBe validatedOrder.billingAddress

                    it.orderLines.padZip(validatedOrder.orderLines)
                        .map { (actual, expected) ->
                            actual.shouldNotBeNull()
                            expected.shouldNotBeNull()
                            with(actual) {
                                orderLineId shouldBe expected.orderLineId
                                productCode shouldBe expected.productCode
                                quantity shouldBe expected.quantity
                                price shouldBe expected.quantity * priceForOrders
                            }
                        }

                    val expectedAmountToBillValue = validatedOrder.orderLines
                        .foldLeft(BigDecimal.ZERO) { acc, line ->
                            acc + when (val qty = line.quantity) {
                                is OrderQuantity.UnitQuantity -> PRODUCT_PRICE_FOR_TESTS * qty.value.toBigDecimal()
                                is OrderQuantity.KilogramQuantity -> PRODUCT_PRICE_FOR_TESTS * qty.value
                            }
                        }
                    it.amountToBill.value shouldBe expectedAmountToBillValue
                }
        }
    }

    "Pricing Order fails" {
        val gppFail: GetProductPrice = { effect { shift(PriceNotInCatalog("$it not found in catalog")) } }

        val order = priceOrder(gppFail, validatedOrder)

        order.shouldBeError()
            .also {
                it.message shouldStartWith "${validatedOrder.orderLines.head.productCode} not found"
            }
    }
})