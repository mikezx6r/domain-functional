package ca.mikewilkes.tdd

import io.kotest.core.spec.style.FreeSpec
import io.kotest.data.row
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith
import io.kotest.matchers.types.shouldBeInstanceOf

class EmailAddressTest : FreeSpec({
    "successfully built" - {
        listOf(
            row("valid", "george@me.cm"),
            row("valid with period", "george.monkey@me.com")
        ).map { (description: String, input: String?) ->
            description {
                val s = Customer.EmailAddress(input)
                s.shouldBeValue()
                    .also {
                        it.value shouldBe input
                    }
            }
        }
    }

    "fails to construct" - {
        listOf(
            row("null", null),
            row("empty", ""),
            row("blank", "   "),
            row("bad email - no @", "george.me.com"),
            row("bad email - nothing before @", "@me.com"),
            row("bad email - nothing after @", "george@")
        ).map { (description: String, input: String?) ->
            description {
                val s = Customer.EmailAddress(input)
                s.shouldBeError()
                    .shouldBeInstanceOf<PlaceOrderError.ValidationError.InvalidEmailAddress>()
                    .also {
                        it.message shouldStartWith "EmailAddress must "
                    }
            }
        }
    }
})