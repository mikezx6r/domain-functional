package ca.mikewilkes.tdd

import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidOrderId
import io.kotest.core.spec.style.FreeSpec
import io.kotest.data.row
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith
import io.kotest.matchers.types.shouldBeInstanceOf

class OrderIdTest : FreeSpec({
    val orderIdMaxLength = 50

    "successfully built" - {
        listOf(
            row("random String", "a4dff"),
            row("Max length string", "A".repeat(orderIdMaxLength))
        ).map { (description: String, input: String?) ->
            description {
                val s = Order.OrderId(input)

                s.shouldBeValue()
                    .also {
                        it.value shouldBe input
                    }
            }
        }
    }

    "fails to construct" - {
        listOf(
            row("null", null),
            row("empty", ""),
            row("blank", "   "),
            row("too long", "S".repeat(orderIdMaxLength) + "4")
        ).map { (description: String, input: String?) ->
            description {
                val s = Order.OrderId(input)
                s.shouldBeError()
                    .shouldBeInstanceOf<InvalidOrderId>()
                    .also {
                        it.message shouldStartWith "OrderId must not be"
                    }
            }
        }
    }

})