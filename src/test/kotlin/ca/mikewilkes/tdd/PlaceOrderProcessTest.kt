package ca.mikewilkes.tdd

import arrow.core.Nel
import arrow.core.continuations.Effect
import arrow.core.continuations.effect
import arrow.core.toNonEmptyListOrNull
import ca.mikewilkes.tdd.Addressee.CheckedAddress
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError
import ca.mikewilkes.tdd.PlaceOrderEvent.BillableOrderPlaced
import ca.mikewilkes.tdd.PlaceOrderEvent.OrderAcknowledgementSent
import ca.mikewilkes.tdd.PlaceOrderEvent.ShippableOrderPlaced
import io.kotest.assertions.arrow.core.shouldContainAll
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith
import io.kotest.matchers.types.shouldBeInstanceOf

class PlaceOrderProcessTest : FreeSpec({
    val caeSuccess: CheckAddressExists = { effect { CheckedAddress(it) } }
    val cpceSuccess: CheckProductCodeExists = { true }
    val gppSuccess: GetProductPrice = { effect { Price(PRODUCT_PRICE_FOR_TESTS) } }
    val coalSuccess: CreateOrderAcknowledgementLetter = { HtmlString("acknowledgement letter") }
    val soaSuccess: SendOrderAcknowledgement = { SendResult.Sent }
    suspend fun placeOrderDI(unvalidatedOrder: Order.UnvalidatedOrder): Effect<PlaceOrderError, Nel<PlaceOrderEvent>> =
        placeOrder(caeSuccess, cpceSuccess, gppSuccess, coalSuccess, soaSuccess, unvalidatedOrder)

    val unvalidatedOrder = TestModels.validUnvalidatedOrder

    "Successfully place an order" {
        val expectedOrder = TestModels.validPricedOrderWithShipping
        val expectedPricedOrder = expectedOrder.pricedOrder
        val expectedAmountToBill =
            BillingAmount.sumPrices(
                unvalidatedOrder.orderLines
                    .map { Price(it.quantity?.toBigDecimal()?.times(PRODUCT_PRICE_FOR_TESTS)!!) }.toNonEmptyListOrNull()
                    ?: throw IndexOutOfBoundsException("Empty list doesn't contain element at index 0.")
            )

        val result = placeOrderDI(unvalidatedOrder)

        result.shouldBeValue()
            .shouldContainAll(
                ShippableOrderPlaced(expectedOrder),
                OrderAcknowledgementSent(expectedPricedOrder.orderId, expectedPricedOrder.customerInfo.email),
                BillableOrderPlaced(
                    expectedPricedOrder.orderId,
                    expectedPricedOrder.billingAddress,
                    expectedAmountToBill
                )
            )
    }

    // Test one case of validation failure, but leave others to comprehensive tests in ValidateOrderProcessTest
    "Order validation failure" {
        val result = placeOrderDI(unvalidatedOrder.copy(orderId = "57".repeat(26)))

        result.shouldBeError()
            .shouldBeInstanceOf<ValidationError>()
            .also {
                it.message shouldStartWith "OrderId must not be"
            }
    }

    // TODO: Use a data-driven test for this to reduce duplication, and make shipping table clearer
    "Calculate Shipping costs" - {
        "US Local delivery" {
            val usLocal =
                unvalidatedOrder.shippingAddress.copy(country = "US", provState = "CA")
            val order = unvalidatedOrder.copy(shippingAddress = usLocal)

            val result = placeOrderDI(order)

            result.shouldBeValue()
                .firstOrNull { it is ShippableOrderPlaced }
                .shouldNotBeNull()
                .shouldBeInstanceOf<ShippableOrderPlaced>()
                .pricedOrderWithShipping.shippingInfo.cost shouldBe Price(5.toBigDecimal())
        }

        "US Remote delivery" {
            val usRemote =
                unvalidatedOrder.shippingAddress.copy(country = "US", provState = "NY")
            val order = unvalidatedOrder.copy(shippingAddress = usRemote)

            val result = placeOrderDI(order)

            result.shouldBeValue()
                .firstOrNull { it is ShippableOrderPlaced }
                .shouldNotBeNull()
                .shouldBeInstanceOf<ShippableOrderPlaced>()
                .pricedOrderWithShipping.shippingInfo.cost shouldBe Price(10.toBigDecimal())
        }

        "International delivery" {
            val international =
                unvalidatedOrder.shippingAddress.copy(country = "CA", provState = "QC")
            val order = unvalidatedOrder.copy(shippingAddress = international)

            val result = placeOrderDI(order)

            result.shouldBeValue()
                .firstOrNull { it is ShippableOrderPlaced }
                .shouldNotBeNull()
                .shouldBeInstanceOf<ShippableOrderPlaced>()
                .pricedOrderWithShipping.shippingInfo.cost shouldBe Price(20.toBigDecimal())
        }
    }
})
