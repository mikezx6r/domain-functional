package ca.mikewilkes.tdd

import ca.mikewilkes.tdd.OrderLine.OrderQuantity
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidQuantity
import io.kotest.core.spec.style.FreeSpec
import io.kotest.data.row
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith
import io.kotest.matchers.types.shouldBeInstanceOf
import java.math.BigDecimal

class OrderQuantityTest : FreeSpec({
    "Unit quantity" - {
        "builds successfully" - {
            listOf(
                row("smallest unit", "1", 1),
                row("value in between", "545", 545),
                row("largest unit", "1000", 1000)
            ).map { (description, value: String?, expected: Int) ->
                description {
                    val result = OrderQuantity.UnitQuantity(value)

                    result.shouldBeValue()
                        .shouldBeInstanceOf<OrderQuantity.UnitQuantity>()
                        .also {
                            it.value shouldBe expected
                        }
                }
            }
        }

        "fails build" - {
            listOf(
                row("not a number", "a1233"),
                row("not an integer", "1.2"),
                row("negative", "-1"),
                row("too small", "0"),
                row("too large", "1001")
            ).map { (description, value: String?) ->
                description {
                    val result = OrderQuantity.UnitQuantity(value)

                    result.shouldBeError()
                        .shouldBeInstanceOf<InvalidQuantity>()
                        .also {
                            it.message shouldStartWith "UnitQuantity must"
                        }
                }
            }
        }
    }

    "Kilogram quantity" - {
        "builds successfully" - {
            listOf(
                row("smallest unit", "0.05", .05.toBigDecimal()),
                row("value in between", "20.05", 20.05.toBigDecimal()),
                row("largest unit", "100", 100.toBigDecimal())
            ).map { (description, value: String?, expected: BigDecimal) ->
                description {
                    val result = OrderQuantity.KilogramQuantity(value)

                    result.shouldBeValue()
                        .shouldBeInstanceOf<OrderQuantity.KilogramQuantity>()
                        .also {
                            it.value shouldBe expected
                        }
                }
            }
        }

        "fails build" - {
            listOf(
                row("not a number", "a1233"),
                row("negative", "-1"),
                row("too small", ".049"),
                row("too large", "100.001")
            ).map { (description, value: String?) ->
                description {

                    val result = OrderQuantity.KilogramQuantity(value)
                    result.shouldBeError()
                        .shouldBeInstanceOf<InvalidQuantity>()
                        .also {
                            it.message shouldStartWith "KilogramQuantity must"
                        }
                }
            }
        }
    }
})
