package ca.mikewilkes.tdd

import arrow.core.continuations.effect
import arrow.core.partially1
import arrow.core.toNonEmptyListOrNull
import ca.mikewilkes.tdd.Addressee.Address
import ca.mikewilkes.tdd.Addressee.UnvalidatedAddress
import ca.mikewilkes.tdd.Order.UnvalidatedOrder
import ca.mikewilkes.tdd.Order.ValidatedOrder
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.AddressCheckFailed
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidObject.InvalidAddress
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidObject.InvalidCustomerInfo
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidObject.InvalidOrderLines
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidOrderId
import io.kotest.core.spec.style.FreeSpec
import io.kotest.data.row
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith
import io.kotest.matchers.types.shouldBeInstanceOf

class ValidateOrderProcessTest : FreeSpec({
    val validUnvalidatedOrder = TestModels.validUnvalidatedOrder

    val caeSuccess: CheckAddressExists = { effect { Addressee.CheckedAddress(it) } }
    val cpceSuccess: CheckProductCodeExists = { true }

    val validateOrderDI = ::validateOrder.partially1(caeSuccess).partially1(cpceSuccess)

    "Validate Order succeeds" - {
        fun verifyOrder(actual: ValidatedOrder, expected: UnvalidatedOrder) {
            fun verifyAddress(actual: Address, expected: UnvalidatedAddress) {
                fun verifyNullableLine(actual: String50?, expected: String?) {
                    if (expected == null) {
                        actual.shouldBeNull()
                    } else {
                        actual?.value shouldBe expected
                    }
                }

                with(actual) {
                    line1.value shouldBe expected.line1
                    verifyNullableLine(line2, expected.line2)
                    verifyNullableLine(line3, expected.line3)
                    verifyNullableLine(line4, expected.line4)
                    city.value shouldBe expected.city
                    provState.toString() shouldBe expected.provState
                    country.toString() shouldBe expected.country
                }
            }

            actual.orderId.value shouldBe expected.orderId

            // Verify Customer Info
            with(actual.customerInfo) {
                name.firstName.value shouldBe expected.customerInfo.firstName
                name.lastName.value shouldBe expected.customerInfo.lastName
                email.value shouldBe expected.customerInfo.email
            }

            // Verify Addresses
            verifyAddress(actual.shippingAddress, expected.shippingAddress)
            verifyAddress(actual.billingAddress, expected.billingAddress)

            // Verify Order Lines
            actual.orderLines.padZip(
                expected.orderLines.toNonEmptyListOrNull()
                    ?: throw IndexOutOfBoundsException("Empty list doesn't contain element at index 0.")
            )
                .map { (actual, expected) ->
                    actual.shouldNotBeNull()
                    expected.shouldNotBeNull()
                    with(actual) {
                        orderLineId.value shouldBe expected.orderLineId
                        productCode.code shouldBe expected.productCode
                        quantity.value.toString() shouldBe expected.quantity
                    }
                }
            actual.orderLines.toList().forEachIndexed { index, line ->
                val expectedLine = expected.orderLines[index]

                line.orderLineId.value shouldBe expectedLine.orderLineId
                line.productCode.code shouldBe expectedLine.productCode
                line.quantity.value.toString() shouldBe expectedLine.quantity
            }
        }

        "Validates successfully with minimal information" {
            val result = validateOrderDI(validUnvalidatedOrder)

            result.shouldBeValue()
                .also {
                    verifyOrder(it, validUnvalidatedOrder)
                }
        }

        "Validates successfully with full information" {
            val fullAddress = UnvalidatedAddress("line1", "line2", "line3", "line4", "city full", "US", "AZ", "64738")
            val unvalidatedOrder =
                validUnvalidatedOrder.copy(shippingAddress = fullAddress, billingAddress = fullAddress)
            val result = validateOrderDI(unvalidatedOrder)

            result.shouldBeValue()
                .also {
                    verifyOrder(it, unvalidatedOrder)
                }
        }
    }

    "Validate Order fails" - {
        "Invalid Order Id" - {
            listOf(
                row("null", null),
                row("too long", "TT".repeat(26))
            ).map { (description, value) ->
                description {
                    val invalidOrder = validUnvalidatedOrder.copy(value)

                    val result = validateOrderDI(invalidOrder)

                    result.shouldBeError()
                        .shouldBeInstanceOf<InvalidOrderId>()
                        .also {
                            it.message shouldStartWith "OrderId must not be "
                        }
                }
            }
        }

        "Invalid CustomerInfo" - {
            val validCustomerInfo = validUnvalidatedOrder.customerInfo

            "Invalid first name" {
                val invalidOrder = validUnvalidatedOrder.copy(
                    customerInfo = validCustomerInfo.copy(firstName = "ab".repeat(26))
                )

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidCustomerInfo>()
                    .also {
                        it.field shouldBe "firstName"
                        it.cause.message shouldStartWith "String50 must not be more than 50 chars."
                    }
            }

            "Invalid last name" {
                val invalidOrder = validUnvalidatedOrder.copy(
                    customerInfo = validCustomerInfo.copy(lastName = "ab".repeat(26))
                )

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidCustomerInfo>()
                    .also {
                        it.field shouldBe "lastName"
                        it.cause.message shouldStartWith "String50 must not be more than 50 chars."
                    }
            }

            "Invalid email" {
                val invalidOrder =
                    validUnvalidatedOrder.copy(customerInfo = validCustomerInfo.copy(email = "ab"))

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidCustomerInfo>()
                    .also {
                        it.field shouldBe "email"
                        it.cause.message shouldStartWith "EmailAddress must match the pattern"
                    }
            }
        }

        "Invalid Shipping Address" - {
            val validAddressInfo = validUnvalidatedOrder.shippingAddress

            "Address Check failure" {
                val caeFailure: CheckAddressExists = { effect { shift(AddressCheckFailed("Address not found")) } }

                val result = validateOrder(caeFailure, cpceSuccess, validUnvalidatedOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidAddress>()
                    .also {
                        it.field shouldBe "existCheck"
                    }
            }

            "Invalid line 1" {
                val invalidOrder =
                    validUnvalidatedOrder.copy(shippingAddress = validAddressInfo.copy(line1 = "AB".repeat(26)))

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidAddress>()
                    .also {
                        it.field shouldBe "line1"
                        it.cause.message shouldStartWith "String50 must not be more than 50 chars."
                    }
            }

            "Invalid line 2" {
                val invalidOrder =
                    validUnvalidatedOrder.copy(shippingAddress = validAddressInfo.copy(line2 = "AB".repeat(26)))

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidAddress>()
                    .also {
                        it.field shouldBe "line2"
                        it.cause.message shouldStartWith "String50 must not be more than 50 chars."
                    }
            }

            "Invalid line 3" {
                val invalidOrder =
                    validUnvalidatedOrder.copy(shippingAddress = validAddressInfo.copy(line3 = "AB".repeat(26)))

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidAddress>()
                    .also {
                        it.field shouldBe "line3"
                        it.cause.message shouldStartWith "String50 must not be more than 50 chars."
                    }
            }

            "Invalid line 4" {
                val invalidOrder =
                    validUnvalidatedOrder.copy(shippingAddress = validAddressInfo.copy(line4 = "AB".repeat(26)))

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidAddress>()
                    .also {
                        it.field shouldBe "line4"
                        it.cause.message shouldStartWith "String50 must not be more than 50 chars."
                    }
            }

            "Invalid line City" {
                val invalidOrder =
                    validUnvalidatedOrder.copy(
                        shippingAddress = validAddressInfo.copy(
                            city = "somewhere else".repeat(
                                5
                            )
                        )
                    )

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidAddress>()
                    .also {
                        it.field shouldBe "city"
                        it.cause.message shouldStartWith "String50 must not be more than 50 chars."
                    }
            }

            "Invalid line Province/State" {
                val invalidOrder =
                    validUnvalidatedOrder.copy(shippingAddress = validAddressInfo.copy(provState = "somewhere else"))

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidAddress>()
                    .also {
                        it.field shouldBe "prov/state"
                        it.cause.message shouldStartWith "ProvState code must be a valid value"
                    }
            }

            "Invalid line Post/Zip" {
                val invalidOrder =
                    validUnvalidatedOrder.copy(shippingAddress = validAddressInfo.copy(postZip = "AB".repeat(26)))

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidAddress>()
                    .also {
                        it.field shouldBe "post/zip"
                        it.cause.message shouldStartWith "PostZip must not be more than 50 chars."
                    }
            }

            "Invalid line Country" {
                val invalidOrder =
                    validUnvalidatedOrder.copy(shippingAddress = validAddressInfo.copy(country = "ABC"))

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidAddress>()
                    .also {
                        it.field shouldBe "country"
                        it.cause.message shouldStartWith "Country code must be a valid value"
                    }
            }
        }

        "Invalid Order Lines" - {
            val unvalidatedOrderLine1 = validUnvalidatedOrder.orderLines[0]
            val unvalidatedOrderLine2 = validUnvalidatedOrder.orderLines[1]

            "No items in the order" {
                val invalidOrder = validUnvalidatedOrder.copy(orderLines = emptyList())

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidOrderLines>()
                    .also {
                        it.field shouldBe "lines"
                        it.cause.message shouldStartWith "There must be at least one item in the Order."
                    }
            }

            "Invalid line id" {
                val invalidOrder = validUnvalidatedOrder.copy(
                    orderLines = listOf(
                        unvalidatedOrderLine1,
                        unvalidatedOrderLine2.copy(orderLineId = "abc".repeat(20))
                    )
                )

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidOrderLines>()
                    .also {
                        it.field shouldBe "orderLineId"
                        it.cause.message shouldStartWith "OrderLineId must not be more than 50 chars."
                    }
            }

            "Product Code" - {
                "Does not exist" {
                    val cpceFails: CheckProductCodeExists = { false }
                    val result = validateOrder(caeSuccess, cpceFails, validUnvalidatedOrder)
                    result.shouldBeError()
                        .shouldBeInstanceOf<InvalidOrderLines>()
                        .also {
                            it.field shouldBe "productCode"
                            it.cause.message shouldStartWith "Product Code not found"
                        }
                }

                "Is invalid" {
                    val invalidOrder = validUnvalidatedOrder.copy(
                        orderLines = listOf(
                            unvalidatedOrderLine1,
                            unvalidatedOrderLine2.copy(productCode = "W234")
                        )
                    )

                    val result = validateOrderDI(invalidOrder)
                    result.shouldBeError()
                        .shouldBeInstanceOf<InvalidOrderLines>()
                        .also {
                            it.field shouldBe "productCode"
                            it.cause.message shouldStartWith "Widget code must start with"
                        }
                }
            }

            "Invalid quantity for product code" {
                val invalidOrder = validUnvalidatedOrder.copy(
                    orderLines = listOf(unvalidatedOrderLine1.copy(quantity = "1.5"), unvalidatedOrderLine2)
                )

                val result = validateOrderDI(invalidOrder)
                result.shouldBeError()
                    .shouldBeInstanceOf<InvalidOrderLines>()
                    .also {
                        it.field shouldBe "quantity"
                        it.cause.message shouldStartWith "UnitQuantity must be an integer"
                    }
            }
        }
    }
})