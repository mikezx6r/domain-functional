package ca.mikewilkes.tdd

import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidString50
import io.kotest.core.spec.style.FreeSpec
import io.kotest.data.row
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith
import io.kotest.matchers.types.shouldBeInstanceOf

class String50Test : FreeSpec({
    val string50MaxLength = 50

    "successfully built" - {
        listOf(
            row("random String", "a4dff"),
            row("Max length string", "A".repeat(string50MaxLength))
        ).map { (description: String, input: String?) ->
            description {
                val s = String50(input)

                s.shouldBeValue()
                    .also {
                        it.value shouldBe input
                    }
            }
        }
    }

    "fails to construct" - {
        listOf(
            row("null", null),
            row("empty", ""),
            row("blank", "   "),
            row("too long", "S".repeat(string50MaxLength) + "4")
        ).map { (description: String, input: String?) ->
            description {
                val s = String50(input)
                s.shouldBeError()
                    .shouldBeInstanceOf<InvalidString50>()
                    .also {
                        it.message shouldStartWith "String50 must not be"
                    }
            }
        }
    }
})