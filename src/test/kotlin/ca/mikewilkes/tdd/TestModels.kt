package ca.mikewilkes.tdd

import arrow.core.continuations.Effect
import arrow.core.continuations.effect
import arrow.core.continuations.merge
import arrow.core.toNonEmptyListOrNull
import ca.mikewilkes.tdd.Addressee.Address
import ca.mikewilkes.tdd.Addressee.UnvalidatedAddress
import ca.mikewilkes.tdd.Customer.CustomerInfo
import ca.mikewilkes.tdd.Customer.PersonalName
import ca.mikewilkes.tdd.Customer.UnvalidatedCustomerInfo
import ca.mikewilkes.tdd.Order.OrderId
import ca.mikewilkes.tdd.Order.PricedOrder
import ca.mikewilkes.tdd.Order.PricedOrderWithShipping
import ca.mikewilkes.tdd.Order.UnvalidatedOrder
import ca.mikewilkes.tdd.Order.ValidatedOrder
import ca.mikewilkes.tdd.OrderLine.UnvalidatedOrderLine
import ca.mikewilkes.tdd.OrderLine.ValidatedOrderLine
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.math.BigDecimal

private const val ORDER_ID = "O12345"
private const val FIRST_NAME = "First"
private const val LAST_NAME = "Last"

private val EMAIL = "$FIRST_NAME$LAST_NAME@$FIRST_NAME-$LAST_NAME.com".lowercase()
private const val ADDR_LINE_1 = "Line 1"
private const val ADDR_CITY = "City"
private const val ADDR_COUNTRY = "CA"
private const val ADDR_PROV_STATE = "ON"
private const val ADDR_POST_ZIP = "K1L2M3"
private const val OL1_ID = "L1"
private const val OL1_PRODUCT = "W1234"
private const val OL1_QUANTITY = "3"
private const val OL2_ID = "L2"
private const val OL2_PRODUCT = "G123"
private const val OL2_QUANTITY = "1.05"
val PRODUCT_PRICE_FOR_TESTS: BigDecimal = 12.5.toBigDecimal()

private data class ValidatedInformation(
    val orderId: OrderId,
    val customerInfo: CustomerInfo,
    val address: Address
)

@OptIn(DelicateCoroutinesApi::class) object TestModels {
    private val validatedInformation: Deferred<Effect<ValidationError, ValidatedInformation>> = GlobalScope.async {
        effect {
            val orderId = OrderId(ORDER_ID).bind()
            val customerInfo = CustomerInfo(
                PersonalName(String50(FIRST_NAME).bind(), String50(LAST_NAME).bind()),
                Customer.EmailAddress(EMAIL).bind()
            )

            val address = Address(
                String50(ADDR_LINE_1).bind(),
                null,
                null,
                null,
                String50(ADDR_CITY).bind(),
                Addressee.Country(ADDR_COUNTRY).bind(),
                Addressee.ProvState(ADDR_PROV_STATE).bind(),
                Addressee.PostZip(ADDR_POST_ZIP).bind()
            )

            ValidatedInformation(orderId, customerInfo, address)
        }
    }

    val validUnvalidatedOrder: UnvalidatedOrder by lazy {
        val unvalidatedCustomerInfo = UnvalidatedCustomerInfo(FIRST_NAME, LAST_NAME, EMAIL)
        val address =
            UnvalidatedAddress(ADDR_LINE_1, null, null, null, ADDR_CITY, ADDR_COUNTRY, ADDR_PROV_STATE, ADDR_POST_ZIP)
        UnvalidatedOrder(
            ORDER_ID, unvalidatedCustomerInfo,
            address,
            address,
            listOf(
                UnvalidatedOrderLine(OL1_ID, OL1_PRODUCT, OL1_QUANTITY),
                UnvalidatedOrderLine(OL2_ID, OL2_PRODUCT, OL2_QUANTITY)
            )
        )
    }

    val validValidatedOrder: Deferred<Effect<ValidationError, ValidatedOrder>> = GlobalScope.async {
        effect {
            val validatedInfo = validatedInformation.getOrFail()
            ValidatedOrder(
                validatedInfo.orderId,
                validatedInfo.customerInfo,
                validatedInfo.address,
                validatedInfo.address,
                listOf(
                    ValidatedOrderLine(
                        OrderLine.OrderLineId(OL1_ID).bind(),
                        OrderLine.ProductCode(OL1_PRODUCT).bind(),
                        OrderLine.OrderQuantity.UnitQuantity(OL1_QUANTITY).bind()
                    ),
                    ValidatedOrderLine(
                        OrderLine.OrderLineId(OL2_ID).bind(),
                        OrderLine.ProductCode(OL2_PRODUCT).bind(),
                        OrderLine.OrderQuantity.KilogramQuantity(OL2_QUANTITY).bind()
                    )
                )
                    .toNonEmptyListOrNull()
                    ?: throw IndexOutOfBoundsException("Empty list doesn't contain element at index 0.")
            )
        }
    }

    val validPricedOrderWithShipping: PricedOrderWithShipping by lazy {
        PricedOrderWithShipping(validPricedOrder.getOrFail(), Order.ShippingInfo(Price(20.toBigDecimal())))
    }

    val validPricedOrder: Deferred<Effect<ValidationError, PricedOrder>> = GlobalScope.async {
        effect {
            val orderLines = listOf(
                OrderLine.PricedOrderLine(
                    OrderLine.OrderLineId(OL1_ID).bind(),
                    OrderLine.ProductCode(OL1_PRODUCT).bind(),
                    OrderLine.OrderQuantity.UnitQuantity(OL1_QUANTITY).bind(),
                    Price(OL1_QUANTITY.toBigDecimal() * PRODUCT_PRICE_FOR_TESTS)
                ),
                OrderLine.PricedOrderLine(
                    OrderLine.OrderLineId(OL2_ID).bind(),
                    OrderLine.ProductCode(OL2_PRODUCT).bind(),
                    OrderLine.OrderQuantity.KilogramQuantity(OL2_QUANTITY).bind(),
                    Price(OL2_QUANTITY.toBigDecimal() * PRODUCT_PRICE_FOR_TESTS)
                )
            )
                .toNonEmptyListOrNull()
                ?: throw IndexOutOfBoundsException("Empty list doesn't contain element at index 0.")
            val validatedInfo = validatedInformation.getOrFail()
            PricedOrder(
                validatedInfo.orderId,
                validatedInfo.customerInfo,
                validatedInfo.address,
                validatedInfo.address,
                orderLines,
                BillingAmount.sumPrices(orderLines.map { it.price })
            )
        }
    }
}

fun <T> Deferred<Effect<ValidationError, T>>.getOrFail(): T = runBlocking {
    this@getOrFail.await()
        .getOrFail()
}

suspend fun <T> Effect<ValidationError, T>.getOrFail(): T =
    this.handleError { throw IllegalStateException("Fatal error. OrderId should have been constructed") }.merge()

