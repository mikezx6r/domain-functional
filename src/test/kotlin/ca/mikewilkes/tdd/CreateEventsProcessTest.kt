package ca.mikewilkes.tdd

import ca.mikewilkes.tdd.PlaceOrderEvent.BillableOrderPlaced
import ca.mikewilkes.tdd.PlaceOrderEvent.OrderAcknowledgementSent
import ca.mikewilkes.tdd.PlaceOrderEvent.ShippableOrderPlaced
import io.kotest.assertions.arrow.core.shouldContainAll
import io.kotest.core.spec.style.FreeSpec

class CreateEventsProcessTest : FreeSpec({
    val pricedOrderWithShipping = TestModels.validPricedOrderWithShipping
    val pricedOrder = pricedOrderWithShipping.pricedOrder
    val orderId = pricedOrder.orderId

    val acknowledgementSent = OrderAcknowledgementSent(orderId, pricedOrder.customerInfo.email)

    "Create Events succeeds" - {
        "All events created when an acknowledgement was sent, and the amount to bill is non-zero" {
            val result = createEvents(pricedOrderWithShipping, acknowledgementSent)

            result.shouldContainAll(
                ShippableOrderPlaced(pricedOrderWithShipping),
                acknowledgementSent,
                BillableOrderPlaced(orderId, pricedOrder.billingAddress, pricedOrder.amountToBill)
            )
        }

        "All events created when acknowledgement was not sent, and the amount to bill is non-zero" {
            val result = createEvents(pricedOrderWithShipping, null)

            result.shouldContainAll(
                ShippableOrderPlaced(pricedOrderWithShipping),
                BillableOrderPlaced(orderId, pricedOrder.billingAddress, pricedOrder.amountToBill)
            )
        }

        "All events created when an acknowledgement was sent, but there is no amount to bill" {
            val noBillAmount =
                pricedOrderWithShipping.copy(pricedOrder = pricedOrder.copy(amountToBill = BillingAmount("0").getOrFail()))

            val result = createEvents(noBillAmount, acknowledgementSent)

            result.shouldContainAll(ShippableOrderPlaced(noBillAmount), acknowledgementSent)
        }

        "All events created when acknowledgement was not sent, and there is no amount to bill" {
            val noBillAmount = pricedOrderWithShipping.copy(
                pricedOrder = pricedOrder.copy(amountToBill = BillingAmount("0").getOrFail())
            )

            val result = createEvents(noBillAmount, null)

            result.shouldContainAll(ShippableOrderPlaced(noBillAmount))
        }
    }
})