package ca.mikewilkes.tdd

import arrow.core.toNonEmptyListOrNull
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class BillingAmountTest : FreeSpec({

    "Sum prices succeeds" {
        val prices = listOf("12.50", "14.27", ".27").map { Price(it.toBigDecimal()) }.toNonEmptyListOrNull()
            ?: throw IndexOutOfBoundsException("Empty list doesn't contain element at index 0.")
        val sum = BillingAmount.sumPrices(prices)

        sum.value shouldBe (12.5 + 14.27 + .27).toBigDecimal()
    }
})