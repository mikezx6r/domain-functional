package ca.mikewilkes.tdd

import arrow.core.continuations.Effect
import io.kotest.assertions.arrow.core.shouldBeLeft
import io.kotest.assertions.arrow.core.shouldBeRight


suspend fun <R, A> Effect<R, A>.shouldBeValue(): A {
    return toEither().shouldBeRight()
}

suspend fun <R, A> Effect<R, A>.shouldBeError(): R {
    return toEither().shouldBeLeft()
}