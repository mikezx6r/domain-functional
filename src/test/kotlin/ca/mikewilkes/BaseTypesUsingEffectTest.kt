@file:Suppress("MethodOverloading")

package ca.mikewilkes

import ca.mikewilkes.tdd.shouldBeError
import ca.mikewilkes.tdd.shouldBeValue
import io.kotest.core.spec.style.FreeSpec
import io.kotest.data.row
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith
import java.math.BigDecimal

private const val MIN = 2
private const val MAX = 10

class BaseTypesUsingEffectTest : FreeSpec({
    "Create String tests" - {
        fun subjectCall(input: String?) = createString("MyString", ::MyString, MAX, input)

        "successfully built" - {
            listOf(
                row("random String", "a4dff"),
                row("Max length string", "A".repeat(MAX))
            ).map { (description: String, input: String?) ->
                description {
                    val s = subjectCall(input)

                    s.shouldBeValue()
                        .also {
                            it.value shouldBe input
                        }
                }
            }
        }

        "fails to construct" - {
            listOf(
                row("null", null),
                row("empty", ""),
                row("blank", "   "),
                row("too long", "S".repeat(MAX) + "4")
            ).map { (description: String, input: String?) ->
                description {
                    val s = subjectCall(input)
                    s.shouldBeError()
                        .shouldStartWith("MyString must not be")
                }
            }
        }
    }


    "Create String nullable tests" - {
        fun subjectCall(input: String?) = createStringNullable("MyString", ::MyString, MAX, input)

        "successfully built" - {
            "null or blank" - {
                listOf(
                    row("null", null),
                    row("empty", ""),
                    row("blank", "   ")
                ).map { (description: String, input: String?) ->
                    description {
                        val s = subjectCall(input)

                        s.shouldBeValue()
                            .shouldBeNull()
                    }
                }
            }

            "other values" - {
                listOf(
                    row("random String", "a4dff"),
                    row("Max length string", "A".repeat(MAX))
                ).map { (description: String, input: String?) ->
                    description {
                        val s = subjectCall(input)

                        s.shouldBeValue()
                            .also {
                                it?.value shouldBe input
                            }
                    }
                }
            }
        }

        "fails to construct" - {
            listOf(
                row("too long", "S".repeat(MAX) + "4")
            ).map { (description: String, input: String?) ->
                description {
                    val s = subjectCall(input)
                    s.shouldBeError()
                        .shouldStartWith("MyString must not be")
                }
            }
        }
    }

    "Create Like tests" - {
        fun subjectCall(input: String?) = createLike("MyString", ::MyString, ".+@.+".toRegex(), input)

        "successfully built" - {
            listOf(
                row("valid", "george@me.cm"),
                row("valid with period", "george.monkey@me.com")
            ).map { (description: String, input: String?) ->
                description {
                    val s = subjectCall(input)
                    s.shouldBeValue()
                        .also {
                            it.value shouldBe input
                        }
                }
            }
        }

        "fails to construct" - {
            listOf(
                row("null", null),
                row("empty", ""),
                row("blank", "   "),
                row("bad email - no @", "george.me.com"),
                row("bad email - nothing before @", "@me.com"),
                row("bad email - nothing after @", "george@")
            ).map { (description: String, input: String?) ->
                description {
                    val s = subjectCall(input)
                    s.shouldBeError()
                        .shouldStartWith("MyString must ")
                }
            }
        }
    }

    "Create Decimal tests" - {
        fun subjectCall(input: String?) =
            createDecimal("MyBigDecimal", ::MyBigDecimal, MIN.toBigDecimal(), MAX.toBigDecimal(), input)

        "successfully built" - {
            listOf(
                row("integer not too small", MIN.toString()),
                row("integer not too large", MAX.toString()),
                row("decimal not too small", (MIN + 0.0001).toString()),
                row("decimal not too large", (MAX - 0.0001).toString())
            ).map { (description: String, input: String) ->
                description {
                    val s = subjectCall(input)
                    s.shouldBeValue()
                        .also {
                            it.value.toString() shouldBe input
                        }
                }
            }
        }

        "fails to construct" - {
            listOf(
                row("null", null),
                row("empty", ""),
                row("blank", "   "),
                row("integer too small", (MIN - 1).toString()),
                row("integer too large", (MAX + 1).toString()),
                row("decimal too small", (MIN - 0.0001).toString()),
                row("decimal too large", (MAX + 0.0001).toString()),
                row("not numeric", "a123")
            ).map { (description: String, input: String?) ->
                description {
                    val s = subjectCall(input)
                    s.shouldBeError()
                        .shouldStartWith("MyBigDecimal must ")
                }
            }
        }
    }

    "Create Int tests" - {
        fun subjectCall(input: String?) = createInt("MyInt", ::MyInt, MIN, MAX, input)

        "successfully built" - {
            listOf(
                row("valid", MIN.toString()),
                row("valid with period", MAX.toString()),
                row("somewhere in between", ((MAX - MIN) / 2).toString())
            ).map { (description: String, input: String?) ->
                description {
                    val s = subjectCall(input)
                    s.shouldBeValue()
                        .also {
                            it.value.toString() shouldBe input
                        }
                }
            }
        }

        "fails to construct" - {
            listOf(
                row("null", null),
                row("empty", ""),
                row("blank", "   "),
                row("too small", (MIN - 1).toString()),
                row("too large", (MAX + 1).toString()),
                row("not integer small", (MIN + 0.00011).toString()),
                row("not numeric", "a123")
            ).map { (description: String, input: String?) ->
                description {
                    val s = subjectCall(input)
                    s.shouldBeError()
                        .shouldStartWith("MyInt must ")
                }
            }
        }
    }

    "Create Enum tests" - {
        fun subjectCall(input: String?) = createEnum("MyEnum", MyEnum::valueOf, input)

        "successfully built" - {
            listOf(
                row("valid lower value 1", "value_1"),
                row("valid lower value 2", "value_2"),
                row("valid upper value 1", "VALUE_1"),
                row("valid upper value 2", "VALUE_2")
            ).map { (description: String, input: String?) ->
                description {
                    val s = subjectCall(input)
                    s.shouldBeValue()
                        .toString() shouldBe input.uppercase()
                }
            }
        }

        "fails to construct" - {
            listOf(
                row("null", null),
                row("empty", ""),
                row("blank", "   "),
                row("bad value", "non-existent value")
            ).map { (description: String, input: String?) ->
                description {
                    val s = subjectCall(input)
                    s.shouldBeError()
                        .shouldStartWith("MyEnum code must ")
                }
            }
        }
    }
})

@Suppress("UseDataClass")
private class MyString(val value: String)

@Suppress("UseDataClass")
private class MyBigDecimal(val value: BigDecimal)

@Suppress("UseDataClass")
private class MyInt(val value: Int)

private enum class MyEnum {
    VALUE_1, VALUE_2
}
