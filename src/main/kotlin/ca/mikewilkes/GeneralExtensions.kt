package ca.mikewilkes

fun String.isNumeric(): Boolean = this.matches(Regex("[0-9]+"))
fun String.isNotNumeric(): Boolean = !this.isNumeric()
