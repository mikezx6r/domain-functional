package ca.mikewilkes.tdd

import arrow.core.continuations.Effect
import arrow.core.continuations.effect
import arrow.core.toNonEmptyListOrNull
import arrow.core.traverse
import ca.mikewilkes.tdd.Order.PricedOrder
import ca.mikewilkes.tdd.Order.ValidatedOrder
import ca.mikewilkes.tdd.OrderLine.PricedOrderLine
import ca.mikewilkes.tdd.OrderLine.ProductCode
import ca.mikewilkes.tdd.OrderLine.ValidatedOrderLine
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.PricingError

typealias GetProductPrice = (ProductCode) -> Effect<PricingError, Price>

suspend fun priceOrder(gpp: GetProductPrice, validatedOrder: ValidatedOrder): Effect<PricingError, PricedOrder> =
    effect {
        val lines = checkNotNull(validatedOrder.orderLines.traverse {
            toPricedOrderLine(gpp, it).bind()
        }?.toNonEmptyListOrNull())
        val amountToBill = BillingAmount.sumPrices(lines.map { it.price })
        PricedOrder(
            validatedOrder.orderId,
            validatedOrder.customerInfo,
            validatedOrder.shippingAddress,
            validatedOrder.billingAddress,
            lines,
            amountToBill
        )
    }

suspend fun toPricedOrderLine(gpp: GetProductPrice, line: ValidatedOrderLine): Effect<PricingError, PricedOrderLine> =
    effect {
        val price = gpp(line.productCode).bind()
        val linePrice = line.quantity * price
        PricedOrderLine(line.orderLineId, line.productCode, line.quantity, linePrice)
    }
