@file:Suppress("DataClassPrivateConstructor", "unused")

package ca.mikewilkes.tdd

import arrow.core.Eval.Companion.now
import arrow.core.Nel
import arrow.core.continuations.Effect
import arrow.core.continuations.EffectScope
import arrow.core.continuations.effect
import ca.mikewilkes.createDecimal
import ca.mikewilkes.createEnum
import ca.mikewilkes.createInt
import ca.mikewilkes.createLike
import ca.mikewilkes.createString
import ca.mikewilkes.createStringNullable
import ca.mikewilkes.isNotNumeric
import ca.mikewilkes.mapError
import ca.mikewilkes.tdd.Addressee.Address
import ca.mikewilkes.tdd.Addressee.UnvalidatedAddress
import ca.mikewilkes.tdd.Customer.CustomerInfo
import ca.mikewilkes.tdd.Customer.EmailAddress
import ca.mikewilkes.tdd.Customer.UnvalidatedCustomerInfo
import ca.mikewilkes.tdd.Order.OrderId
import ca.mikewilkes.tdd.Order.PricedOrder
import ca.mikewilkes.tdd.Order.PricedOrderWithShipping
import ca.mikewilkes.tdd.OrderLine.PricedOrderLine
import ca.mikewilkes.tdd.OrderLine.UnvalidatedOrderLine
import ca.mikewilkes.tdd.OrderLine.ValidatedOrderLine
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidCountry
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidEmailAddress
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidOrderId
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidOrderLineId
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidPostZip
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidProductCode
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidQuantity
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidStateProv
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidString50
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.PricingError.InvalidBillingAmount
import java.math.BigDecimal

// Dependency definitions
typealias CreateOrderAcknowledgementLetter = (PricedOrder) -> HtmlString
typealias SendOrderAcknowledgement = (OrderAcknowledgement) -> SendResult

sealed class Order {
    data class UnvalidatedOrder(
        val orderId: String?,
        val customerInfo: UnvalidatedCustomerInfo,
        val shippingAddress: UnvalidatedAddress,
        val billingAddress: UnvalidatedAddress,
        val orderLines: List<UnvalidatedOrderLine>
    ) : Order()

    data class ValidatedOrder(
        val orderId: OrderId,
        val customerInfo: CustomerInfo,
        val shippingAddress: Address,
        val billingAddress: Address,
        val orderLines: Nel<ValidatedOrderLine>
    ) : Order()

    data class PricedOrder(
        val orderId: OrderId,
        val customerInfo: CustomerInfo,
        val shippingAddress: Address,
        val billingAddress: Address,
        val orderLines: Nel<PricedOrderLine>,
        val amountToBill: BillingAmount
    ) : Order()

    data class PricedOrderWithShipping(
        val pricedOrder: PricedOrder,
        val shippingInfo: ShippingInfo
    )

    data class OrderId private constructor(val value: String) {
        companion object {
            suspend operator fun invoke(value: String?): Effect<InvalidOrderId, OrderId> =
                createString("OrderId", ::OrderId, 50, value)
                    .mapError { InvalidOrderId(it) }
        }
    }

    data class ShippingInfo(val cost: Price)
}

sealed class PlaceOrderError {
    sealed class ValidationError(val message: String) : PlaceOrderError() {
        class InvalidString50(message: String) : ValidationError(message)
        class InvalidOrderId(message: String) : ValidationError(message)
        class InvalidOrderLineId(message: String) : ValidationError(message)
        class InvalidEmailAddress(message: String) : ValidationError(message)
        class InvalidProductCode(message: String) : ValidationError(message)
        class InvalidQuantity(message: String) : ValidationError(message)
        class EmptyNonEmptyList(message: String) : ValidationError(message)
        class AddressCheckFailed(message: String) : ValidationError(message)
        class InvalidCountry(message: String) : ValidationError(message)
        class InvalidStateProv(message: String) : ValidationError(message)
        class InvalidPostZip(message: String) : ValidationError(message)

        sealed class InvalidObject(val field: String, val cause: ValidationError) : ValidationError("$field - $cause") {

            class InvalidCustomerInfo(field: String, cause: ValidationError) : InvalidObject(field, cause)

            class InvalidAddress(field: String, cause: ValidationError) : InvalidObject(field, cause)

            class InvalidOrderLines(field: String, cause: ValidationError) : InvalidObject(field, cause)
        }

        sealed class PricingError(message: String) : ValidationError(message) {
            class PriceNotInCatalog(message: String) : PricingError(message)
            class InvalidBillingAmount(message: String) : PricingError(message)
        }

        override fun toString(): String {
            return "ValidationError(message='$message')"
        }
    }
}

sealed class PlaceOrderEvent {
    // Event to send to billing context
    // Will only be created if the AmountToBill is not zero
    @Suppress("MaxLineLength")
    data class BillableOrderPlaced(val orderId: OrderId, val billingAddress: Address, val amountToBill: BillingAmount) :
        PlaceOrderEvent()

    data class OrderAcknowledgementSent(val orderId: OrderId, val emailAddress: EmailAddress) : PlaceOrderEvent()
    data class ShippableOrderPlaced(val pricedOrderWithShipping: PricedOrderWithShipping) : PlaceOrderEvent()
}

sealed class Customer {
    data class UnvalidatedCustomerInfo(
        val firstName: String?,
        val lastName: String?,
        val email: String?
    ) : Customer()

    data class CustomerInfo(val name: PersonalName, val email: EmailAddress) : Customer()

    // Part of Customer info BUT not a sub-class of Customer. Put here to keep it together
    data class PersonalName(val firstName: String50, val lastName: String50)
    data class EmailAddress private constructor(val value: String) {
        companion object {
            suspend operator fun invoke(email: String?): Effect<InvalidEmailAddress, EmailAddress> =
                createLike("EmailAddress", ::EmailAddress, ".+@.+".toRegex(), email)
                    .mapError { InvalidEmailAddress(it) }
        }
    }
}

sealed class Addressee {
    data class UnvalidatedAddress(
        val line1: String?,
        val line2: String?,
        val line3: String?,
        val line4: String?,
        val city: String?,
        val country: String?,
        val provState: String?,
        val postZip: String?
    ) : Addressee()

    data class CheckedAddress(val address: UnvalidatedAddress) : Addressee()

    data class Address(
        val line1: String50,
        val line2: String50?,
        val line3: String50?,
        val line4: String50?,
        val city: String50,
        val country: Country,
        val provState: ProvState,
        val postZip: PostZip
    ) : Addressee()

    enum class Country {
        US, CA;

        companion object {
            suspend operator fun invoke(value: String?): Effect<InvalidCountry, Country> {
                return createEnum("Country", ::valueOf, value)
                    .mapError { InvalidCountry(it) }
            }
        }
    }

    enum class ProvState {
        CA, OR, AZ, NV, NY,
        ON, BC, QC, MB;

        companion object {
            suspend operator fun invoke(value: String?): Effect<InvalidStateProv, ProvState> =
                createEnum("ProvState", ProvState::valueOf, value)
                    .mapError { InvalidStateProv(it) }
        }
    }

    data class PostZip private constructor(val value: String) {
        companion object {
            suspend operator fun invoke(value: String?): Effect<InvalidPostZip, PostZip> =
                createString("PostZip", ::PostZip, 50, value)
                    .mapError { InvalidPostZip(it) }
        }
    }
}

@Suppress("DataClassPrivateConstructor")
data class BillingAmount private constructor(val value: BigDecimal) {
    companion object {
        suspend operator fun invoke(value: String?): Effect<InvalidBillingAmount, BillingAmount> =
            createDecimal("BillingAmount", ::BillingAmount, BigDecimal.ZERO, 10000.toBigDecimal(), value)
                .mapError {
                    InvalidBillingAmount(it)
                }

        fun sumPrices(prices: Nel<Price>): BillingAmount =
            BillingAmount(prices.foldRight(now(BigDecimal.ZERO)) { price, acc ->
                now(acc.value() + price.value)
            }.value())
    }
}

@Suppress("MethodOverloading")
sealed class OrderLine {
    data class UnvalidatedOrderLine(
        val orderLineId: String?,
        val productCode: String?,
        val quantity: String?
    ) :
        OrderLine()

    data class ValidatedOrderLine(
        val orderLineId: OrderLineId,
        val productCode: ProductCode,
        val quantity: OrderQuantity<*>
    ) : OrderLine()

    data class PricedOrderLine(
        val orderLineId: OrderLineId,
        val productCode: ProductCode,
        val quantity: OrderQuantity<*>,
        val price: Price
    ) : OrderLine()

    data class OrderLineId private constructor(val value: String) {
        companion object {
            suspend operator fun invoke(value: String?): Effect<InvalidOrderLineId, OrderLineId> =
                createString("OrderLineId", ::OrderLineId, 50, value)
                    .mapError { InvalidOrderLineId(it) }
        }
    }

    /**
     * Product Code which can be a Widget or a Gadget.
     *
     * Widget codes start with W, followed by 4 digits
     * Gadget codes start with G, followed by 3 digits
     */
    @Suppress("DataClassPrivateConstructor")
    sealed class ProductCode {
        abstract val code: String

        data class Widget private constructor(override val code: String) : ProductCode() {
            companion object {
                suspend operator fun invoke(code: String): Effect<InvalidProductCode, Widget> = effect {
                    buildVerify("Widget", ::Widget, 'W', 4, code)
                }
            }
        }

        data class Gadget private constructor(override val code: String) : ProductCode() {
            companion object {
                suspend operator fun invoke(code: String): Effect<InvalidProductCode, Gadget> = effect {
                    buildVerify("Gadget", ::Gadget, 'G', 3, code)
                }
            }
        }

        companion object {
            /**
             * Using invoke operator allows code to use `ProductCode` like a constructor, but ultimately end up with the
             * validations being applied, and the correct type of ProductCode being returned.
             */
            suspend operator fun invoke(code: String?): Effect<ValidationError, ProductCode> = effect {
                when {
                    code.isNullOrBlank() -> shift(InvalidProductCode("Code must have a value"))
                    code.startsWith("W") -> Widget(code).bind()
                    code.startsWith("G") -> Gadget(code).bind()
                    else -> shift(InvalidProductCode("Unknown Product code: '$code'."))
                }
            }

            /*
             * Helper function that ensures the passed code starts with the correct initChar, is only digits after the
             * initial character and that the digits are of the correct length.
             *
             * If everything passes, it will use the ctor passed to construct a Right(T).
             * If there is a validation failure, a Left(String) will be returned
             */
            context(EffectScope<InvalidProductCode>)
                    private suspend fun <T> buildVerify(
                name: String,
                ctor: (String) -> T,
                initChar: Char,
                digitLength: Int,
                code: String
            ): T {
                ensure(
                    code.length == digitLength + 1 &&
                            !code.takeLast(digitLength).isNotNumeric() &&
                            code.startsWith(initChar)
                ) {
                    InvalidProductCode(
                        "$name code must start with '$initChar' followed by $digitLength digits. '$code'"
                    )
                }
                return ctor(code)
            }
        }
    }

    /**
     * Order Quantity which can be units or kilograms.
     *
     * Units are integer, and between 1 and 1000
     * Kilograms are decimal, and between 0.05 kg and 100 kg
     */
    @Suppress("DataClassPrivateConstructor")
    sealed class OrderQuantity<T> {
        abstract val value: T

        // Use operator overloading to provide a reasonable meaning for `*` in code
        abstract operator fun times(price: Price): Price

        data class UnitQuantity private constructor(override val value: Int) : OrderQuantity<Int>() {
            override operator fun times(price: Price): Price = Price(price.value * value.toBigDecimal())

            companion object {
                suspend operator fun invoke(value: String?): Effect<InvalidQuantity, UnitQuantity> =
                    createInt("UnitQuantity", ::UnitQuantity, 1, 1000, value)
                        .mapError { InvalidQuantity(it) }
            }
        }

        data class KilogramQuantity private constructor(override val value: BigDecimal) : OrderQuantity<BigDecimal>() {
            override operator fun times(price: Price): Price = Price(price.value * value)

            companion object {
                suspend operator fun invoke(value: String?): Effect<InvalidQuantity, KilogramQuantity> =
                    createDecimal(
                        "KilogramQuantity",
                        ::KilogramQuantity,
                        0.05.toBigDecimal(),
                        100.toBigDecimal(),
                        value
                    ).mapError { InvalidQuantity(it) }
            }
        }
    }
}

sealed class SendResult {
    object Sent : SendResult()
    object NotSent : SendResult()
}

@JvmInline value class Price(val value: BigDecimal)

@JvmInline value class HtmlString(val value: String)

@Suppress("DataClassPrivateConstructor")
data class String50 private constructor(val value: String) {
    companion object {
        suspend operator fun invoke(value: String?): Effect<ValidationError, String50> =
            createString("String50", ::String50, 50, value)
                .mapError { InvalidString50(it) }

        suspend fun nullable(value: String?): Effect<ValidationError, String50?> =
            createStringNullable("String50", ::String50, 50, value)
                .mapError {
                    InvalidString50(it)
                }
    }
}
