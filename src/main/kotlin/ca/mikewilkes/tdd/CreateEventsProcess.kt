package ca.mikewilkes.tdd

import arrow.core.Nel
import arrow.core.toNonEmptyListOrNull
import ca.mikewilkes.tdd.Order.PricedOrder
import ca.mikewilkes.tdd.Order.PricedOrderWithShipping
import ca.mikewilkes.tdd.PlaceOrderEvent.BillableOrderPlaced
import ca.mikewilkes.tdd.PlaceOrderEvent.OrderAcknowledgementSent
import ca.mikewilkes.tdd.PlaceOrderEvent.ShippableOrderPlaced
import java.math.BigDecimal

typealias CreateEvents = (PricedOrderWithShipping, OrderAcknowledgementSent?) -> Nel<PlaceOrderEvent>

val createEvents: CreateEvents = { pricedOrderWithShipping, orderAcknowledgementSent ->
    listOfNotNull(
        ShippableOrderPlaced(pricedOrderWithShipping),
        orderAcknowledgementSent,
        createBillingEvent(pricedOrderWithShipping.pricedOrder)
    )
        .toNonEmptyListOrNull() ?: throw IndexOutOfBoundsException("Empty list doesn't contain element at index 0.")
}

// Create event only if the amount to bill is greater than zero
private fun createBillingEvent(placedOrder: PricedOrder): BillableOrderPlaced? =
    placedOrder.amountToBill.takeIf { it.value > BigDecimal.ZERO }
        ?.let { amountToBill ->
            BillableOrderPlaced(placedOrder.orderId, placedOrder.billingAddress, amountToBill)
        }
