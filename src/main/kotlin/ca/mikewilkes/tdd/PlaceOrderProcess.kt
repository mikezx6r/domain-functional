package ca.mikewilkes.tdd

import arrow.core.Nel
import arrow.core.continuations.Effect
import arrow.core.continuations.effect
import ca.mikewilkes.tdd.Order.UnvalidatedOrder

@Suppress("LongParameterList")
suspend fun placeOrder(
    cae: CheckAddressExists,
    cpce: CheckProductCodeExists,
    gpp: GetProductPrice,
    coal: CreateOrderAcknowledgementLetter,
    soa: SendOrderAcknowledgement,
    unvalidatedOrder: UnvalidatedOrder
): Effect<PlaceOrderError, Nel<PlaceOrderEvent>> = effect {
    val validatedOrder = validateOrder(cae, cpce, unvalidatedOrder).bind()

    // Validation has succeeded
    val pricedOrder = priceOrder(gpp, validatedOrder).bind()

    val pricedOrderWithShipping = addShippingInfoToOrder(pricedOrder)
    // Pricing has succeeded
    val orderAcknowledgementSent = acknowledgeOrder(coal, soa, pricedOrder)


    createEvents(pricedOrderWithShipping, orderAcknowledgementSent)
}