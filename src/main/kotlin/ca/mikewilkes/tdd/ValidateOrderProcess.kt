package ca.mikewilkes.tdd

import arrow.core.Option
import arrow.core.continuations.Effect
import arrow.core.continuations.effect
import arrow.core.left
import arrow.core.toNonEmptyListOrNull
import ca.mikewilkes.mapError
import ca.mikewilkes.tdd.Addressee.Address
import ca.mikewilkes.tdd.Addressee.CheckedAddress
import ca.mikewilkes.tdd.Addressee.UnvalidatedAddress
import ca.mikewilkes.tdd.Customer.CustomerInfo
import ca.mikewilkes.tdd.Customer.PersonalName
import ca.mikewilkes.tdd.Customer.UnvalidatedCustomerInfo
import ca.mikewilkes.tdd.Order.UnvalidatedOrder
import ca.mikewilkes.tdd.Order.ValidatedOrder
import ca.mikewilkes.tdd.OrderLine.OrderQuantity
import ca.mikewilkes.tdd.OrderLine.ProductCode
import ca.mikewilkes.tdd.OrderLine.ValidatedOrderLine
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.AddressCheckFailed
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.EmptyNonEmptyList
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidObject.InvalidAddress
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidObject.InvalidCustomerInfo
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidObject.InvalidOrderLines
import ca.mikewilkes.tdd.PlaceOrderError.ValidationError.InvalidProductCode

// TODO Make this explicitly an Async Call
typealias CheckAddressExists = (UnvalidatedAddress) -> Effect<AddressCheckFailed, CheckedAddress>

typealias CheckProductCodeExists = (ProductCode) -> Boolean

/**
 * Validate an input order to ensure it is correct.
 *
 * - Order id
 * - Customer Info
 * - Shipping info
 * - Billing info
 * - Order Lines
 */
suspend fun validateOrder(
    cae: CheckAddressExists,
    cpce: CheckProductCodeExists,
    unvalidatedOrder: UnvalidatedOrder
): Effect<ValidationError, ValidatedOrder> =
    effect {
        val orderId = Order.OrderId(unvalidatedOrder.orderId).bind()
        val customerInfo = toCustomerInfo(unvalidatedOrder.customerInfo).bind()

        val shippingAddress = toAddress(cae, unvalidatedOrder.shippingAddress).bind()
        val billingAddress = toAddress(cae, unvalidatedOrder.billingAddress).bind()

        val orderLines = Option.fromNullable(unvalidatedOrder.orderLines.map { toValidatedOrderLine(cpce, it).bind() }
            .toNonEmptyListOrNull()).bind {
            InvalidOrderLines("lines", EmptyNonEmptyList("There must be at least one item in the Order."))
        }

        ValidatedOrder(orderId, customerInfo, shippingAddress, billingAddress, orderLines)
    }

private suspend fun toCustomerInfo(customerInfo: UnvalidatedCustomerInfo): Effect<InvalidCustomerInfo, CustomerInfo> {
    return effect {
        CustomerInfo(
            PersonalName(
                String50(customerInfo.firstName)
                    .mapError { InvalidCustomerInfo("firstName", it) }
                    .bind(),
                String50(customerInfo.lastName)
                    .mapError { InvalidCustomerInfo("lastName", it) }
                    .bind()
            ),
            Customer.EmailAddress(customerInfo.email)
                .mapError { InvalidCustomerInfo("email", it) }
                .bind()
        )
    }
}

private suspend fun toAddress(cae: CheckAddressExists, unvalidatedAddress: UnvalidatedAddress):
        Effect<ValidationError, Address> {
    return effect {
        val checkedAddress = (cae(unvalidatedAddress).mapError { InvalidAddress("existCheck", it) })
            .bind().address

        Address(
            String50(checkedAddress.line1).mapError { InvalidAddress("line1", it) }.bind(),
            String50.nullable(checkedAddress.line2).mapError { InvalidAddress("line2", it) }.bind(),
            String50.nullable(checkedAddress.line3).mapError { InvalidAddress("line3", it) }.bind(),
            String50.nullable(checkedAddress.line4).mapError { InvalidAddress("line4", it) }.bind(),
            String50(checkedAddress.city).mapError { InvalidAddress("city", it) }.bind(),
            Addressee.Country(checkedAddress.country).mapError { InvalidAddress("country", it) }
                .bind(),
            Addressee.ProvState(checkedAddress.provState).mapError { InvalidAddress("prov/state", it) }
                .bind(),
            Addressee.PostZip(checkedAddress.postZip).mapError { InvalidAddress("post/zip", it) }.bind()
        )
    }
}

private suspend fun toValidatedOrderLine(cpce: CheckProductCodeExists, line: OrderLine.UnvalidatedOrderLine):
        Effect<ValidationError, ValidatedOrderLine> {
    suspend fun toProductCode(productCode: String?): Effect<ValidationError, ProductCode> =
        effect {
            @Suppress("IMPLICIT_NOTHING_TYPE_ARGUMENT_IN_RETURN_POSITION")
            ProductCode(productCode).bind().takeIf(cpce)
                ?: InvalidProductCode("Product Code not found: $productCode").left().bind()
        }

    suspend fun toOrderQuantity(
        productCode: ProductCode,
        quantity: String?
    ): Effect<ValidationError, OrderQuantity<*>> =
        when (productCode) {
            is ProductCode.Widget -> OrderQuantity.UnitQuantity(quantity)
            is ProductCode.Gadget -> OrderQuantity.KilogramQuantity(quantity)
        }

    return effect {
        val orderLineId = OrderLine.OrderLineId(line.orderLineId).mapError { InvalidOrderLines("orderLineId", it) }
        val productCode = toProductCode(line.productCode).mapError { InvalidOrderLines("productCode", it) }
        val quantity = toOrderQuantity(productCode.bind(), line.quantity).mapError { InvalidOrderLines("quantity", it) }

        ValidatedOrderLine(orderLineId.bind(), productCode.bind(), quantity.bind())
    }
}
