@file:Suppress("KDocMissingDocumentation")

package ca.mikewilkes.tdd

import ca.mikewilkes.tdd.Order.PricedOrder
import ca.mikewilkes.tdd.PlaceOrderEvent.OrderAcknowledgementSent

@Suppress("MaxLineLength")
typealias AcknowledgeOrder = (CreateOrderAcknowledgementLetter, SendOrderAcknowledgement, PricedOrder) -> OrderAcknowledgementSent?

val acknowledgeOrder: AcknowledgeOrder = { coal, soa, pricedOrder ->
    val letter = coal(pricedOrder)

    val acknowledgment = OrderAcknowledgement(pricedOrder.customerInfo.email, letter)

    when (soa(acknowledgment)) {
        SendResult.Sent -> OrderAcknowledgementSent(pricedOrder.orderId, pricedOrder.customerInfo.email)
        SendResult.NotSent -> null
    }
}

data class OrderAcknowledgement(val emailAddress: Customer.EmailAddress, val letter: HtmlString)
