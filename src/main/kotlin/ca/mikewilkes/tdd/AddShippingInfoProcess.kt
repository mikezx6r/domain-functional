package ca.mikewilkes.tdd

import ca.mikewilkes.tdd.Addressee.Address
import ca.mikewilkes.tdd.Addressee.Country
import ca.mikewilkes.tdd.Addressee.ProvState.AZ
import ca.mikewilkes.tdd.Addressee.ProvState.CA
import ca.mikewilkes.tdd.Addressee.ProvState.NV
import ca.mikewilkes.tdd.Addressee.ProvState.OR
import ca.mikewilkes.tdd.Order.PricedOrder
import ca.mikewilkes.tdd.Order.PricedOrderWithShipping
import ca.mikewilkes.tdd.Order.ShippingInfo
import ca.mikewilkes.tdd.ShippingCategory.INTERNATIONAL
import ca.mikewilkes.tdd.ShippingCategory.US_LOCAL_STATE
import ca.mikewilkes.tdd.ShippingCategory.US_REMOTE_STATE

private typealias AddShippingInfoToOrder = (PricedOrder) -> PricedOrderWithShipping

val addShippingInfoToOrder: AddShippingInfoToOrder = {
    val shippingInfo = ShippingInfo(determineShippingCost(it))
    PricedOrderWithShipping(it, shippingInfo)
}

enum class ShippingCategory {
    US_REMOTE_STATE,
    US_LOCAL_STATE,
    INTERNATIONAL
}

private fun determineShippingCost(pricedOrder: PricedOrder): Price =
    when (pricedOrder.shippingAddress.shippingCategory) {
        US_LOCAL_STATE -> 5
        US_REMOTE_STATE -> 10
        INTERNATIONAL -> 20
    }.let {
        Price(it.toBigDecimal())
    }

val Address.shippingCategory: ShippingCategory
    get() = if (country == Country.US) {
        if (provState in listOf(CA, OR, AZ, NV)) US_LOCAL_STATE else US_REMOTE_STATE
    } else {
        INTERNATIONAL
    }
