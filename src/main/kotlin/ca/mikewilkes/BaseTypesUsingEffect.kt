package ca.mikewilkes

import arrow.core.continuations.Effect
import arrow.core.continuations.effect
import java.math.BigDecimal

fun <T> createString(name: String, ctor: (String) -> T, maxLen: Int, value: String?): Effect<String, T> =
    effect {
        when {
            value.isNullOrBlank() -> shift("$name must not be null or blank")
            value.length > maxLen -> shift("$name must not be more than $maxLen chars. '$value'")
            else -> ctor(value)
        }
    }

/**
 * Create a String object that could be Nullable. Empty or null input results in a null return
 */
fun <T> createStringNullable(
    name: String,
    ctor: (String) -> T,
    maxLen: Int,
    value: String?
): Effect<String, T?> =
    effect {
        when {
            value.isNullOrBlank() -> null
            value.length > maxLen -> shift("$name must not be more than $maxLen chars. '$value'")
            else -> ctor(value)
        }
    }

fun <T> createLike(name: String, ctor: (String) -> T, pattern: Regex, value: String?): Effect<String, T> =
    effect {
        when {
            value.isNullOrBlank() -> shift("$name must not be null or blank")
            !pattern.matches(value) -> shift("$name must match the pattern $pattern")
            else -> ctor(value)
        }
    }

fun <T> createDecimal(
    name: String,
    ctor: (BigDecimal) -> T,
    minVal: BigDecimal,
    maxVal: BigDecimal,
    value: String?
): Effect<String, T> =
    effect {
        if (value.isNullOrBlank()) {
            shift("$name must not be null or blank")
        } else {
            try {
                val bd = value.toBigDecimal()
                when {
                    bd < minVal -> shift("$name must be greater than $minVal")
                    bd > maxVal -> shift("$name must be less than $maxVal")
                    else -> ctor(bd)
                }
            } catch (e: NumberFormatException) {
                shift("$name must be a decimal value $value")
            }
        }
    }

fun <T> createInt(name: String, ctor: (Int) -> T, minVal: Int, maxVal: Int, value: String?): Effect<String, T> =
    effect {
        if (value.isNullOrBlank()) {
            shift("$name must not be null or blank")
        } else {
            try {
                val intValue = value.toInt()
                when {
                    intValue < minVal -> shift("$name must be greater than $minVal")
                    intValue > maxVal -> shift("$name must be less than $maxVal")
                    else -> ctor(intValue)
                }
            } catch (e: NumberFormatException) {
                shift("$name must be an integer value $value")
            }
        }
    }

fun <T> createEnum(name: String, ctor: (String) -> T, value: String?): Effect<String, T> =
    effect {
        if (value.isNullOrBlank()) {
            shift("$name code must have a value")
        } else {
            @Suppress("SwallowedException")
            try {
                ctor(value.uppercase())
            } catch (e: IllegalArgumentException) {
                shift("$name code must be a valid value. '$value'.")
            }
        }
    }

suspend fun <R, A, R2> Effect<R, A>.mapError(fn: (R) -> R2): Effect<R2, A> =
    handleErrorWith { effect { shift(fn(it)) } }
